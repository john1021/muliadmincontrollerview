<?php
/**
 *
 * Copyright © 2015 Employeecommerce. All rights reserved.
 */
namespace Police\Entryform\Controller\Index;

use Magento\Backend\App\Action\Context;
use \Magento\Framework\Controller\ResultFactory;
use Police\Entryform\Model\ContactFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_modelContactFactory;
	
	
    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		ContactFactory $modelContactFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
        $this->_messageManager = $messageManager;
		$this->_modelContactFactory = $modelContactFactory;
    }
    
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
		
		 	/* Starts Process For Base URL */
	         $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
             $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
             $baseUrl = $storeManager->getStore()->getBaseUrl();	
			 
			 $red_url=$baseUrl."entryform/"."display/display";
	        /* Finish Process For Base URL */

         $post = $this->getRequest()->getPostValue();
         if(isset($post) && !empty($post)) {

         $emp_name=$post['emp_name'];
         $emp_dept=$post['emp_dept'];
         $emp_add=$post['emp_add'];
         $emp_salary=$post['emp_salary'];
         $emp_joindate=$post['emp_joindate'];

        $contact = $this->_objectManager->create('Police\Entryform\Model\Contact');
        $contact->addData([
            "emp_name" => "$emp_name",
            "emp_dept" => "$emp_dept",
            "emp_add" => "$emp_add",
            "emp_salary" => "$emp_salary",
            "emp_joindate" => "$emp_joindate"
        ]);

        $saveData = $contact->save();

        if($saveData) {
           $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
           $resultRedirect->setUrl("$red_url");
           return $resultRedirect;
          } else {
           $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
          $resultRedirect->setUrl("$red_url");
          }
       
	   
	   
	
	   
        
		
      
       }
	   
	    $this->resultPage = $this->resultPageFactory->create();  
         return $this->resultPage;
      
	  
	  
	  /*$post = $this->_postFactory->create();
		$collection = $post->getCollection();
		foreach($collection as $item){
			echo "<pre>";
			print_r($item->getData());
			echo "</pre>";
		}
		exit();
		return $this->_pageFactory->create();*/

       
     
    }
}
