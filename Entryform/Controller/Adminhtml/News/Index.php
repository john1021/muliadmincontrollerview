<?php
/**
 *
 * Copyright © 2015 Employeecommerce. All rights reserved.
 */
namespace Police\Entryform\Controller\Adminhtml\News;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Police\Entryform\Controller\Adminhtml\News;
class Index extends \Magento\Backend\App\Action
{

	/**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(Context $context,PageFactory $resultPageFactory) {
		
    parent::__construct($context);
    $this->_resultPageFactory = $resultPageFactory;

}
    /**
     * Check the permission to run it
     *
     * @return bool
     */
   /*  protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Cms::page');
    } */

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
         if($this->getRequest()->getQuery('ajax')) {
           $this->_forward('grid');
           return;
           }

         $resultPage = $this->_resultPageFactory->create();
         $resultPage->setActiveMenu('Police_Entryform\::main_menu');
         $resultPage->getConfig()->getTitle()->prepend(__('Florida Book'));
         return $resultPage;
    }
}

