<?php
namespace Police\EntryformController\Adminhtml\News;
use Police\Entryform\Controller\Adminhtml\News;
class NewAction extends News
{
/**
* Create new news action
*
* @return void
*/
public function execute()
{
$this->_forward('edit');
}
}