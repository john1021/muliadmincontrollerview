<?php
namespace Police\Entryform\Block\Adminhtml\News\Edit\Tab;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config;
use Police\Entryform\Model\System\Config\Status;
class Info extends Generic implements TabInterface
{
/**
* @var \Magento\Cms\Model\Wysiwyg\Config
*/
protected $_wysiwygConfig;
/**
* @var \Tutorial\SimpleNews\Model\Config\Status
*/
protected $_newsStatus;
/**
* @param Context $context
* @param Registry $registry
* @param FormFactory $formFactory
* @param Config $wysiwygConfig
* @param Status $newsStatus
* @param array $data
*/
public function __construct(
Context $context,
Registry $registry,
FormFactory $formFactory,
Config $wysiwygConfig,
Status $newsStatus,
array $data = []
) {
$this->_wysiwygConfig = $wysiwygConfig;
$this->_newsStatus = $newsStatus;
parent::__construct($context, $registry, $formFactory, $data);
}
/**
* Prepare form fields
*
* @return \Magento\Backend\Block\Widget\Form
*/
protected function _prepareForm()
{
/** @var $model \Tutorial\SimpleNews\Model\News */
$model = $this->_coreRegistry->registry('entryform_news');
/** @var \Magento\Framework\Data\Form $form */
$form = $this->_formFactory->create();
$form->setHtmlIdPrefix('news_');
$form->setFieldNameSuffix('news');
$fieldset = $form->addFieldset(
'base_fieldset',
['legend' => __('General')]
);
if ($model->getId()) {
$fieldset->addField(
'id',
'hidden',
['name' => 'id']
);
}
$fieldset->addField(
'title',
'text',
[
'name' => 'emp_name',
'label' => __('Employee Name'),
'required' => true
]
);
$fieldset->addField(
'title',
'text',
[
'name' => 'emp_dept',
'label' => __('Employee Dept'),
'required' => true
]
);
$fieldset->addField(
'summary',
'textarea',
[
'name' => 'emp_add',
'label' => __('Employee Address'),
'required' => true,
'style' => 'height: 15em; width: 30em;'
]
);
$fieldset->addField(
'summary',
'text',
[
'name' => 'emp_salary',
'label' => __('Employee Salary'),
'required' => true,
'required' => true
]
);

$data = $model->getData();
$form->setValues($data);
$this->setForm($form);
return parent::_prepareForm();
}
/**
* Prepare label for tab
*
* @return string
*/
public function getTabLabel()
{
return __('News Info');
}
/**
* Prepare title for tab
*
* @return string
*/
public function getTabTitle()
{
return __('News Info');
}
/**
* {@inheritdoc}
*/
public function canShowTab()
{
return true;
}
/**
* {@inheritdoc}
*/
public function isHidden()
{
return false;
}
}