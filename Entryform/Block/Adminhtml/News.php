<?php
/**
 * Copyright © 2015 Police . All rights reserved.
 */
namespace Police\Entryform\Block\Adminhtml;
class News extends \Magento\Backend\Block\Widget\Grid\Container
{

	protected function _construct()
	{
		$this->_controller = 'adminhtml_news';
        $this->_blockGroup = 'Police_Entryform';
        $this->_headerText = __('Manage News');
        $this->_addButtonLabel = __('Add News');
        parent::_construct();
	}
}

